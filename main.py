def media(num1, num2):
    """Calcula a média entre dois números."""
    return (num1 + num2) / 2
